package creationalpatterns.abstractfactory;

public class FordElectricCarFactory extends AbstractElectricCarFactory {

	@Override
	public ElectricCar buyElectricCar() {
		return new FordElectricCar();
	}

}
