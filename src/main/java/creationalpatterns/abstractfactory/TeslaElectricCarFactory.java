package creationalpatterns.abstractfactory;

public class TeslaElectricCarFactory extends AbstractElectricCarFactory {

	@Override
	public ElectricCar buyElectricCar() {

		return new TeslaElectricCar();

	}

}
