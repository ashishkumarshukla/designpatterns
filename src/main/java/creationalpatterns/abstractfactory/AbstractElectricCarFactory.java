package creationalpatterns.abstractfactory;

public abstract class AbstractElectricCarFactory {
	private static final FordElectricCarFactory FORD_FACTORY = new FordElectricCarFactory();
	private static final TeslaElectricCarFactory TESLA_FACTORY = new TeslaElectricCarFactory();

	public abstract ElectricCar buyElectricCar();

	public static AbstractElectricCarFactory getFactory(Company company) {
		AbstractElectricCarFactory factory = null;
		switch (company) {
		case FORD:
			factory = FORD_FACTORY;
			break;
		case TESLA:
			factory = TESLA_FACTORY;
			break;
		}
		return factory;
	}

}
