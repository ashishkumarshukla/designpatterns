package creationalpatterns.factorymethod;

public interface Car {
	
	public void createCar();

}
