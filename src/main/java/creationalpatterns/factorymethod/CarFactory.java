package creationalpatterns.factorymethod;

public class CarFactory {

	public static Car buildCar(CarType carType) {
		Car car = null;
		switch (carType) {
		case ELECTRIC:
			car = new ElectricCar();
			break;
		case TRADITIONAL:
			car = new TraditionalCar();
			break;
		}
		return car;

	}

}
