package creationalpatterns.factorymethod;

public class TraditionalCar implements Car {

	@Override
	public void createCar() {
	}

	public TraditionalCar() {
		createCar();
	}

}
