package creationalpatterns.factorymethod;

public class ElectricCar implements Car {

	@Override
	public void createCar() {

	}

	public ElectricCar() {
		createCar();
	}

}
