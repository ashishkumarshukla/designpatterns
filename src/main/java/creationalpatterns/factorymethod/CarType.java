package creationalpatterns.factorymethod;

public enum CarType {
	ELECTRIC, TRADITIONAL;

}
